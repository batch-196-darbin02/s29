// db.products.insertMany(
//     [
//         {
//             "name":"iPhone X",
//             "price":30000,
//             "isActive":true
//         },
//         {
//             "name":"Samsung Galaxy S21",
//             "price":51000,
//             "isActive":true
//         },
//         {
//             "name":"Razer Blackshark V2X",
//             "price":2800,
//             "isActive":false
//         },
//         {
//             "name":"RAKK Gaming Mouse",
//             "price":1800,
//             "isActive":true
//         },
//         {
//             "name":"Razer Mechanical Keyboard",
//             "price":4000,
//             "isActive":true
//         }
//     ]
// )


// Query Operators
// Allows us to expand our queries and define conditions instead of just looking specific values

// $gt, $lt, $gte, $lte - query operators
// $gt - greater than
// db.products.find({price:{$gt:3000}})
// 
// $lt - lesser than
// db.products.find({price:{$lt:3000}})
// 
// $gte - greater than or equal
// db.products.find({price:{$gte:30000}})
// 
// $lte - lesser than or equal
// db.products.find({price:{$lte:2800}})

db.users.insertMany(
    [
        {
            firstname:"Mary Jane",
            lastname:"Watson",
            email:"mjtiger@gmail.com",
            password:"tigerjackpot15",
            isAdmin:false
        },
        {
            firstname:"Gwen",
            lastname:"Stacy",
            email:"stacyTech@gmail.com",
            password:"stacyTech1992",
            isAdmin:true
        },
        {
            firstname:"Peter",
            lastname:"Parker",
            email:"peterWebDev@gmail.com",
            password:"webdeveloperPeter",
            isAdmin:true
        },
        {
            firstname:"Jonah",
            lastname:"Jameson",
            email:"jjjameson@gmail.com",
            password:"spideyisamenace",
            isAdmin:false
        },
        {
            firstname:"Otto",
            lastname:"Octavius",
            email:"ottoOctopi@gmail.com",
            password:"docOck15",
            isAdmin:true
        }
    ]
)

// $regex - query operator which will allow us to find documents which will match the characters/pattern of the characters we are looking for.
// $regex - looks for documents with partial match and by default is case sensitive
db.users.find({firstname:{$regex:"o"}})

// $options - used our regex will be case insensitive
db.users.find({firstname:{$regex:"o",$options:"$i"}})

// you can also find for documents with partial matches
db.products.find({name:{$regex:"phone",$options:"i"}})

db.users.find({email:{$regex:"web",$options:"i"}})

db.products.find({name:{$regex:"razer",$options:"i"}})
db.products.find({name:{$regex:"rakk",$options:"i"}})

// $or $and - logical operators - works almost the same way as they in JS
// $or - allows us to have a logical operation to find for documents which can satisfy at least 1 of the conditions

db.products.find({$or:[{name:{$regex:"x", $options:"$i"}},{price:{$lte:10000}}]})
db.products.find({$or:[{name:{$regex:"x", $options:"$i"}},{price:{$gte:30000}}]})

// $and - look or find for documents that satisfies all conditions
db.products.find({$and:[{name:{$regex:"razer", $options:"$i"}},{price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex:"x", $options:"$i"}},{price:{$gte:30000}}]})
db.users.find({$and:[{lastname:{$regex:"w", $options:"$i"}},{isAdmin:false}]})
db.users.find({$and:[{firstname:{$regex:"a", $options:"$i"}},{isAdmin:true}]})

// Field Projection - allow us to show/hide certain properties/fields
// db.collection.find({query},{projection}) - 0 means hide, 1 means show
// if query is blank, it refers to all collections

db.users.find({},{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1})

db.users.find({isAdmin:false},{firstname:1,lastname:1})
// by default, _id field is shown if its not specified to be hidden

db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})